<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<section id="content">
				<?php if(have_posts()): while (have_posts()): the_post(); ?>

					<article>
						<div class="featured-image">
							<?php the_post_thumbnail( 'large' ); ?>

							<div class="featured-image__caption">
								<p><?php the_post_thumbnail_caption(); ?></p>
							</div>							
						</div>

						<div class="article-header">

							<div class="flex-wrapper">
								<?php get_template_part('partials/tax'); ?>								

								<?php get_template_part('partials/share'); ?>
							</div>


							<div class="date">
								<h4><?php the_date('F j, Y'); ?></h4>
							</div>

							<div class="title">
								<h1><?php the_title(); ?></h1>
							</div>

							<div class="byline">
								<?php $author = get_field('author'); if($author): ?>
									<h4>by <a href="<?php echo get_permalink($author); ?>"><?php echo get_the_title($author); ?></a></h4>
								<?php endif; ?>
							</div>


						</div>

						<div class="article-body">
							<?php the_content(); ?>									
						</div>

						<div class="article-footer">

							<div class="tags">
								<h4>Tags</h4>
								<p><?php the_tags('', '', ''); ?></p>
							</div>

							<?php get_template_part('partials/share'); ?>

							<div class="author">
								<?php $post_object = get_field('author'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

									<div class="photo">
										<a href="<?php the_permalink(); ?>">
											<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										</a>
									</div>

									<div class="info">
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
										<?php the_field('bio'); ?>
									</div>
										
								<?php wp_reset_postdata(); endif; ?>

							</div>

							<div class="commtents">
								<?php comments_template(); ?> 
							</div>
							
						</div>
					</article>

				<?php endwhile; endif; ?>
			</section>

			<?php get_sidebar(); ?>

		</div>
	</section>

	
<?php get_footer(); ?>