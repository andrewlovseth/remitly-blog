<footer>
	<div class="footer-responsive">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 push-lg-4 col-md-3 col-sm-4">
					<h4>Empresa</h4>

					<ul class="info-list">
						<li><a href="http://bit.ly/RemitlyBlogAbout">Acerca</a></li>
						<li><a href="https://blog.remitly.com/">Blog</a></li>
						<li><a href="http://bit.ly/RemitlyBlogNews">Prensa</a></li>
						<li><a href="http://bit.ly/RemitlyBlogCareers">Empleo</a></li>
					</ul>
				</div>

				<div class="col-lg-2 push-lg-4 col-md-3 col-sm-4">
					<h4>Producto</h4>

					<ul class="info-list">
						<li><a href="http://bit.ly/RemitlyBlogPricing">Tasas y Tarifas</a></li>
						<li><a rel="nofollow" href="http://bit.ly/RemitlyBlogSecurity">Seguridad</a></li>
						<li><a href="http://bit.ly/RemitlyBlogReviews">Reseñas</a></li>
						<li><a href="http://bit.ly/RemitlyBlogPartners">Agentes</a></li>
						<li><a href="http://bit.ly/RemitlyBlogReferrals">Cómo ganar recompensas</a></li>
					</ul>
				</div>

				<div class="col-lg-2 push-lg-4 col-md-3 col-sm-4">
					<h4>Ayuda</h4>

					<ul class="info-list">
						<li><a href="http://bit.ly/RemitlyBlogContact">Contáctanos</a></li>
						<li><a rel="nofollow" href="http://bit.ly/RemitlyBlogComplaints">Danos tu opinión</a></li>
						<li><a href="http://bit.ly/RemitlyBlogFAQ">Preguntas Frecuentes</a></li>
					</ul>
				</div>

				<div class="col-lg-2 push-lg-4 col-md-3 hidden-sm-down">
					<h4>Conectar</h4>
					<div class="icon-row">
					    <a href="https://twitter.com/remitly" target="_blank" title="Twitter">
					        <span class="orca-icon orca-icon-twitter"></span>
					    </a>
					    <a href="https://www.facebook.com/Remitly" target="_blank" title="Facebook">
					        <span class="orca-icon orca-icon-facebook"></span>
					    </a>
					    <a href="https://plus.google.com/114197402907519452377" target="_blank" title="Google Plus">
					        <span class="orca-icon orca-icon-google-plus"></span>
					    </a>
					</div>
					
					<div class="why-i-send-container">
						<a href="https://whyisendmoneyabroad.remitly.com" class="btn why-i-send-button">#WhyISend</a>
					</div>
				</div>

				<div class="col-lg-4 pull-lg-8 col-md-12 hidden-sm-down logo-column">
					<img src="//dqyag3aekzepn.cloudfront.net/assets/logos/logo-full-horizontal-white_es_6f4155bfb3705d51b5b5a7b62ba7ba4d.svg" class="logo" alt="Remitly">
				
					<p class="footer-legal-links">
						<a rel="nofollow" href="http://bit.ly/RemitlyBlogUserAgreement">Acuerdo con el usuario</a><span class="separator"> | </span>
						<a rel="nofollow" href="http://bit.ly/RemitlyBlogPrivacyPolicy">Privacidad</a><span class="separator"> | </span>
						<a href="http://bit.ly/RemitlyBlogLicenses">Licencias</a>
					</p>

					<p>© 2012 - 2018 Remitly, Inc. Todos los derechos reservados.</p>
				
					<div class="app-store-links">
						<a href="http://bit.ly/RemitlyBlogAndroid" target="_blank" title="Google Play">
							<img src="//dqyag3aekzepn.cloudfront.net/assets/appstore-badges/google-play-es_30a28accb416d9dcf8841a0be1d694e4.png" alt="Google Play">
						</a>
						<a href="http://bit.ly/RemitlyBlogiOS" target="_blank" title="App Store">
							<img src="//dqyag3aekzepn.cloudfront.net/assets/appstore-badges/ios-es_bfcd25cf1de398ec5585ccce998d8a7d.svg" alt="App Store">
						</a>
					</div>
				</div>

			</div>
		</div>

		<div class="hidden-md-up">
			<div class="text-center">
				<div class="icon-row">
				    <a href="https://twitter.com/remitly" target="_blank" title="Twitter">
				        <span class="orca-icon orca-icon-twitter"></span>
				    </a>
				    <a href="https://www.facebook.com/Remitly" target="_blank" title="Facebook">
				        <span class="orca-icon orca-icon-facebook"></span>
				    </a>
				    <a href="https://plus.google.com/114197402907519452377" target="_blank" title="Google Plus">
				        <span class="orca-icon orca-icon-google-plus"></span>
				    </a>
				</div>

				<div class="app-store-links">
					<a href="http://bit.ly/RemitlyBlogAndroid" target="_blank" title="Google Play">
						<img src="//dqyag3aekzepn.cloudfront.net/assets/appstore-badges/google-play-es_30a28accb416d9dcf8841a0be1d694e4.png" alt="Google Play">
					</a>
					<a href="http://bit.ly/RemitlyBlogiOS" target="_blank" title="App Store">
						<img src="//dqyag3aekzepn.cloudfront.net/assets/appstore-badges/ios-es_bfcd25cf1de398ec5585ccce998d8a7d.svg" alt="App Store">
					</a>
				</div>

				<div class="why-i-send-container">
					<a href="https://whyisendmoneyabroad.remitly.com" class="btn why-i-send-button">#WhyISend</a>
				</div>
			</div>

			<div class="eula">
				<p class="footer-legal-links">
					<a rel="nofollow" href="http://bit.ly/RemitlyBlogUserAgreement">Acuerdo con el usuario</a><span class="separator"> | </span>
					<a rel="nofollow" href="http://bit.ly/RemitlyBlogPrivacyPolicy">Privacidad</a><span class="separator"> | </span>
					<a href="http://bit.ly/RemitlyBlogLicenses">Licencias</a>
				</p>

				<p>© 2012 - 2018 Remitly, Inc. Todos los derechos reservados.</p>
			</div>
		</div>
	</div>
</footer>