<div class="tax">
	<?php $terms = get_the_terms( get_the_ID(), 'country' ); if ($terms && ! is_wp_error( $terms )) : ?>
		<div class="countries">
			<?php foreach ($terms as $term): ?>

				<a href="<?php echo get_term_link( $term->term_id ); ?>">
					<img src="<?php $image = get_field('flag', 'country_' . $term->term_id); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

			<?php endforeach; ?>
		</div>
	<?php endif; ?>									

	<div class="cat">
		<?php the_category( ' ' ); ?>
	</div>
</div>
