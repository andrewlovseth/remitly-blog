<section class="share">
	<h4>Share</h4>

	<div class="links-wrapper">		
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" class="facebook" rel="external">
			<img src="<?php echo bloginfo('template_directory'); ?>/images/facebook.svg" alt="Facebook" />
		</a>

		<a href="https://twitter.com/intent/tweet/?text=<?php echo urlencode(get_the_title()); ?>&url=<?php echo urlencode(get_permalink()); ?>" class="twitter" rel="external">
			<img src="<?php echo bloginfo('template_directory'); ?>/images/twitter.svg" alt="Twitter" />
		</a>
		
		<div class="link">
			<img src="<?php echo bloginfo('template_directory'); ?>/images/link.svg" alt="Link" />
			<span><?php echo get_permalink(); ?></span>
		</div>
	</div>

</section>