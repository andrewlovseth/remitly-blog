<section id="content-header">

	<?php if(is_category()): ?>
		<h1><?php $current_category = single_cat_title("", false); echo $current_category; ?></h1>
		
		<?php $cat = get_queried_object(); if(get_field('tagline', $cat)): ?>
			<h2><?php the_field('tagline', $cat); ?></h2>
		<?php endif; ?>
	<?php endif; ?>


	<?php if(is_tax('country')): ?>
		<h1 class="country-tax">
			<img src="<?php $term = get_queried_object(); $image = get_field('flag', $term); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<span class="name"><?php $current_tax = single_term_title("", false); echo $current_tax; ?>
		</h1>
	<?php endif; ?>
	

	<?php if(is_search()): ?>
		<h1>Search Results for "<?php echo get_search_query(); ?>"</h1>

		<?php 
			global $wp_query;
			$total_results = $wp_query->found_posts;
		?>

		<?php if($total_results != 0): ?>
			<h2><?php echo $total_results; ?> articles found</h2>
		<?php endif; ?>	

	<?php endif; ?>

</section>

