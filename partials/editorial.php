<?php 
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	if( is_home() && 1 == $paged ):
?>
		
	<?php if(have_rows('homepage_editorial', 'options')): $count = 1; ?>

		<section id="editorial">

			<?php while(have_rows('homepage_editorial', 'options')): the_row(); ?>

				<?php if($count == 1): ?>
					<?php $post_object = get_sub_field('article'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
					
						<article class="hero">
							<div class="photo">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail( 'large' ); ?>
								</a>
							</div>

							<div class="info">
								<?php get_template_part('partials/tax'); ?>

								<div class="date">
									<h4><?php the_time('F j, Y'); ?></h4>
								</div>

								<div class="title">
									<h3>
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</h3>	
								</div>

								<div class="description">
									<?php the_excerpt(); ?>
								</div>									
							</div>
						</article>
						
					<?php wp_reset_postdata(); endif; ?>

				<?php else: ?>

					<?php $post_object = get_sub_field('article'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
					
							<article class="featured">
								<div class="photo">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail( 'large' ); ?>
									</a>
								</div>

								<div class="info">
									<?php get_template_part('partials/tax'); ?>

									<div class="date">
										<h4><?php the_time('F j, Y'); ?></h4>
									</div>

									<div class="title">
										<h3>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</h3>	
									</div>

								</div>
							</article>
						
					<?php wp_reset_postdata(); endif; ?>

				<?php endif; ?>

			<?php $count++; endwhile; ?>

		</section>

	<?php endif; ?>

<?php endif; ?>