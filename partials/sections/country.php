<?php
	$country = get_sub_field('country');
	$display_term = get_term($country);
?>

<section class="three-col country">

	<?php if(get_sub_field('header')): ?>
		<div class="header">
			<h3>
				<img src="<?php $image = get_field('flag', 'country_' . $country); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php the_sub_field('header'); ?>
			</h3>
		</div>
	<?php endif; ?>

	<?php
		$posts_per_page = get_sub_field('number_of_articles_to_show');
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => $posts_per_page,
			'tax_query' => array(
				array(
					'taxonomy' => 'country',
					'field'    => 'term_id',
					'terms'    => $country 
				)
			)
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

		<?php get_template_part('partials/sections/three-col-article'); ?>

	<?php endwhile; endif; wp_reset_postdata(); ?>

	<div class="view-all">
		<a href="<?php echo get_term_link($country); ?>" class="btn">View all <?php echo $display_term->name; ?> posts</a>							
	</div>
	
</section>