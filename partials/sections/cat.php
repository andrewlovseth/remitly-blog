<section class="three-col cat">

	<?php if(get_sub_field('header')): ?>
		<div class="header">
			<h3><?php the_sub_field('header'); ?></h3>
		</div>
	<?php endif; ?>

	<?php
		$cat = get_sub_field('category');
		$posts_per_page = get_sub_field('number_of_articles_to_show');
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => $posts_per_page,
			'cat' => $cat
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

		<?php get_template_part('partials/sections/three-col-article'); ?>

	<?php endwhile; endif; wp_reset_postdata(); ?>

	<div class="view-all">
		<a href="<?php echo get_category_link($cat); ?>" class="btn">View all <?php echo get_cat_name($cat); ?> posts</a>							
	</div>
	
</section>
			