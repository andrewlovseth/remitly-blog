<section class="three-col tag">

	<?php if(get_sub_field('header')): ?>
		<div class="header">
			<h3><?php the_sub_field('header'); ?></h3>
		</div>
	<?php endif; ?>

	<?php
		$tag = get_sub_field('tag');
		$display_tag = get_tag($tag);
		$posts_per_page = get_sub_field('number_of_articles_to_show');
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => $posts_per_page,
			'tag_id' => $tag
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

		<?php get_template_part('partials/sections/three-col-article'); ?>

	<?php endwhile; endif; wp_reset_postdata(); ?>

	<div class="view-all">
		<a href="<?php echo get_tag_link($tag); ?>" class="btn">View all <?php echo $display_tag->name; ?> posts</a>							
	</div>
	
</section>