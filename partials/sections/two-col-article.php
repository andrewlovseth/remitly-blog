<article class="two-col">
	<div class="photo">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( 'large' ); ?>
		</a>
	</div>

	<div class="info">
		<?php get_template_part('partials/tax'); ?>

		<div class="date">
			<h4><?php the_time('F j, Y'); ?></h4>
		</div>

		<div class="title">
			<h3>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h3>	
		</div>

		<div class="description">
			<?php the_excerpt(); ?>
		</div>									
	</div>
</article>