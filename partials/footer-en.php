	<footer>
			
		<div class="footer-responsive">
			<div class="container">
				<div class="row">

					<div class="col-lg-2 push-lg-4 col-md-3 col-sm-4">
						<h4>Company</h4>

						<ul class="info-list">
							<li><a href="http://bit.ly/RemitlyBlogAbout">About</a></li>
							<li><a href="https://blog.remitly.com/">Blog</a></li>
							<li><a href="http://bit.ly/RemitlyBlogNews">Press</a></li>
							<li><a href="http://bit.ly/RemitlyBlogCareers">Careers</a></li>
						</ul>
					</div>

					<div class="col-lg-2 push-lg-4 col-md-3 col-sm-4">
						<h4>Product</h4>

						<ul class="info-list">
							<li><a href="http://bit.ly/RemitlyBlogPricing">Rates &amp; Fees</a></li>
							<li><a href="http://bit.ly/RemitlyBlogSecurity" rel="nofollow">Security</a></li>
							<li><a href="http://bit.ly/RemitlyBlogReviews">Reviews</a></li>
							<li><a href="http://bit.ly/RemitlyBlogPartners">Partners</a></li>
							<li><a href="http://bit.ly/RemitlyBlogReferrals">How to Earn Rewards</a></li>
						</ul>
					</div>

					<div class="col-lg-2 push-lg-4 col-md-3 col-sm-4">
						<h4>Support</h4>

						<ul class="info-list">
							<li><a href="http://bit.ly/RemitlyBlogContact">Contact Us</a></li>
							<li><a href="http://bit.ly/RemitlyBlogComplaints" rel="nofollow">Feedback</a></li>
							<li><a href="http://bit.ly/RemitlyBlogFAQ">FAQ</a></li>
						</ul>
					</div>

					<div class="col-lg-2 push-lg-4 col-md-3 hidden-sm-down">
						<h4>Connect</h4>

						<div class="icon-row">
						    <a href="https://twitter.com/remitly" target="_blank" title="Twitter">
						        <span class="orca-icon orca-icon-twitter"></span>
						    </a>
						    <a href="https://www.facebook.com/Remitly" target="_blank" title="Facebook">
						        <span class="orca-icon orca-icon-facebook"></span>
						    </a>
						    <a href="https://plus.google.com/114197402907519452377" target="_blank" title="Google Plus">
						        <span class="orca-icon orca-icon-google-plus"></span>
						    </a>
						</div>

						<div class="why-i-send-container">
							<a href="https://whyisendmoneyabroad.remitly.com" class="btn why-i-send-button">#WhyISend</a>
						</div>

					</div>

					<div class="col-lg-4 pull-lg-8 col-md-12 hidden-sm-down logo-column">
						<img src="//dqyag3aekzepn.cloudfront.net/assets/logos/logo-full-horizontal-white_2062bbf73e88613dee8238428be77545.svg" class="logo" alt="Remitly">

						<p class="footer-legal-links">
							<a rel="nofollow" href="http://bit.ly/RemitlyBlogUserAgreement">User Agreement</a><span class="separator"> | </span>
							<a rel="nofollow" href="http://bit.ly/RemitlyBlogPrivacyPolicy">Privacy Policy</a><span class="separator"> | </span>
							<a href="http://bit.ly/RemitlyBlogLicenses">Licenses</a>
						</p>

						<p>© 2012 - 2018 Remitly, Inc. All rights reserved.</p>

						<div class="app-store-links">
							<a href="http://bit.ly/RemitlyBlogAndroid" target="_blank" title="Google Play">
								<img src="//dqyag3aekzepn.cloudfront.net/assets/appstore-badges/google-play-en_14c4b411bf67ac5581234f35c4145047.png" alt="Google Play">
							</a>
							<a href="http://bit.ly/RemitlyBlogiOS" target="_blank" title="App Store">
								<img src="//dqyag3aekzepn.cloudfront.net/assets/appstore-badges/ios-en_1e5a2d62df3e996ca7e023e5eeb10b54.svg" alt="App Store">
							</a>
						</div>

					</div>
				</div>
			</div>

			<div class="hidden-md-up">
				<div class="text-center">
					<div class="icon-row">
					    <a href="https://twitter.com/remitly" target="_blank" title="Twitter">
					        <span class="orca-icon orca-icon-twitter"></span>
					    </a>
					    <a href="https://www.facebook.com/Remitly" target="_blank" title="Facebook">
					        <span class="orca-icon orca-icon-facebook"></span>
					    </a>
					    <a href="https://plus.google.com/114197402907519452377" target="_blank" title="Google Plus">
					        <span class="orca-icon orca-icon-google-plus"></span>
					    </a>
					</div>

					<div class="app-store-links">
						<a href="http://bit.ly/RemitlyBlogAndroid" target="_blank" title="Google Play">
							<img src="//dqyag3aekzepn.cloudfront.net/assets/appstore-badges/google-play-en_14c4b411bf67ac5581234f35c4145047.png" alt="Google Play">
						</a>
						<a href="http://bit.ly/RemitlyBlogiOS" target="_blank" title="App Store">
							<img src="//dqyag3aekzepn.cloudfront.net/assets/appstore-badges/ios-en_1e5a2d62df3e996ca7e023e5eeb10b54.svg" alt="App Store">
						</a>
					</div>

					<div class="why-i-send-container">
						<a href="https://whyisendmoneyabroad.remitly.com" class="btn why-i-send-button">#WhyISend</a>
					</div>
				</div>

				<div class="eula">
					<p class="footer-legal-links">
						<a rel="nofollow" href="http://bit.ly/RemitlyBlogUserAgreement">User Agreement</a><span class="separator"> | </span>
						<a rel="nofollow" href="http://bit.ly/RemitlyBlogPrivacyPolicy">Privacy Policy</a><span class="separator"> | </span>
						<a href="http://bit.ly/RemitlyBlogLicenses">Licenses</a>
					</p>

					<p>© 2012 - 2018 Remitly, Inc. All rights reserved.</p>
				</div>
			</div>
		</div>
</footer>