<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<section id="content">

				<div class="author">

					<div class="photo">
						<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="info">
						<h3><?php the_title(); ?></h3>
						<?php the_field('bio'); ?>
					</div>
					
				</div>

				<div class="posts">

					<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => -1,
							'meta_query' => array(
								array(
									'key' => 'author',
									'value' => get_the_ID(),
									'compare' => '='
								)
							)
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

							<?php get_template_part('partials/post'); ?>

					<?php endwhile; endif; wp_reset_postdata(); ?>
										
				</div>

			</section>

			<?php get_sidebar(); ?>

		</div>
	</section>

	
<?php get_footer(); ?>