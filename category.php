<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<section id="content">

				<?php $cat = get_queried_object(); ?>

				<section id="content-header">

					<h1><?php $current_category = single_cat_title("", false); echo $current_category; ?></h1>

					<?php if(get_field('tagline', $cat)): ?>
						<h2><?php the_field('tagline', $cat); ?></h2>
					<?php endif; ?>

				</section>

				<?php if(get_field('editorial_layout', $cat)): ?>

					<?php if(have_rows('sections', $cat)): while(have_rows('sections', $cat)) : the_row(); ?>
	 

					    <?php if( get_row_layout() == 'one_col' ): ?>

						    <section class="one-col">

							    <?php $post_object = get_sub_field('article_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							    	<?php get_template_part('partials/one-col-article'); ?>

							    <?php wp_reset_postdata(); endif; ?>
																		    	
						    </section>

					    <?php endif; ?>


					    <?php if( get_row_layout() == 'two_col' ): ?>

					    	<section class="two-col">

					    		<?php if(get_sub_field('header')): ?>
					    			<div class="header">
					    				<h3><?php the_sub_field('header'); ?></h3>
					    			</div>
						    	<?php endif; ?>

							    <?php $post_object = get_sub_field('article_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							    	<?php get_template_part('partials/two-col-article'); ?>

							    <?php wp_reset_postdata(); endif; ?>

							    <?php $post_object = get_sub_field('article_b'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							    	<?php get_template_part('partials/two-col-article'); ?>

							    <?php wp_reset_postdata(); endif; ?>
					    		
					    	</section>
													
					    <?php endif; ?>

					 
					    <?php if( get_row_layout() == 'three_col' ): ?>

					    	<section class="three-col">

					    		<?php if(get_sub_field('header')): ?>
					    			<div class="header">
					    				<h3><?php the_sub_field('header'); ?></h3>
					    			</div>
						    	<?php endif; ?>

							    <?php $post_object = get_sub_field('article_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							    	<?php get_template_part('partials/three-col-article'); ?>

							    <?php wp_reset_postdata(); endif; ?>

							    <?php $post_object = get_sub_field('article_b'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							    	<?php get_template_part('partials/three-col-article'); ?>

							    <?php wp_reset_postdata(); endif; ?>

							    <?php $post_object = get_sub_field('article_c'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							    	<?php get_template_part('partials/three-col-article'); ?>

							    <?php wp_reset_postdata(); endif; ?>
					    		
					    	</section>
													
					    <?php endif; ?>				 


					    <?php if( get_row_layout() == 'category_feed' ): ?>

							<?php get_template_part('partials/sections/cat'); ?>
													
					    <?php endif; ?>		
		

					    <?php if( get_row_layout() == 'tag_feed' ): ?>

							<?php get_template_part('partials/sections/tag'); ?>
													
					    <?php endif; ?>		
					    
		
					    <?php if( get_row_layout() == 'country_feed' ): ?>'

							<?php get_template_part('partials/sections/country'); ?>
													
					    <?php endif; ?>			


					<?php endwhile; endif; ?>

				<?php else: ?>

					<?php if (have_posts()): ?>

						<div class="posts">

							<?php while (have_posts()): the_post(); ?>

								<?php get_template_part('partials/post'); ?>

							<?php endwhile; ?>

							<?php get_template_part('partials/pagination'); ?>

						</div>

					<?php else: ?>

						<div class="no-results">
							<h2>No posts found</h2>
						</div>

					<?php endif; ?>

				<?php endif; ?>			

			</section>

			<?php get_sidebar(); ?>

		</div>
	</section>
	
<?php get_footer(); ?>