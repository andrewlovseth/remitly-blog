<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" href="https://use.typekit.net/etz4ogn.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<link rel='stylesheet' href='https://media.remitly.io/renderer-component-3rd-party-35e93f87a198f1dca4dd3f005b9c6a33.css' />


	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<header>
		<div class="wrapper">

			<div class="logo">
				<a href="http://bit.ly/RemitlyBlogLogo">
					<img src="<?php $image = get_field('logo', pll_current_language('slug')); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="utilties">

				<div class="user">
					<?php if(pll_current_language() == 'en'): ?>
						<a href="http://bit.ly/RemitlyBlogLogin" class="btn sign-in">Sign In</a>
						<a href="http://bit.ly/RemitlyBlogSignup" class="btn register">Join Now</a>
					<?php elseif(pll_current_language() == 'es'): ?>
						<a href="http://bit.ly/RemitlyBlogLogin" class="btn sign-in">Inicia sesión</a>
						<a href="http://bit.ly/RemitlyBlogSignup" class="btn register">Regístrate</a>
					<?php endif; ?>
				</div>

				<div class="lang-switcher">
					<ul>
						<?php pll_the_languages();?>
					</ul>				
				</div>
			
			</div>

		</div>
	</header>

	<div class="mobile-categories">
		<div class="wrapper">
			
			<section class="topics">
				<h3><?php the_field('topics_header', pll_current_language('slug')); ?></h3>

				<?php $topics = get_field('topics', pll_current_language('slug')); if( $topics ): ?>
					<div class="list">
						<?php foreach( $topics as $topic ): ?>

							<div class="topic">
								<a href="<?php echo get_term_link( $topic->term_id ); ?>">
									<span class="name"><?php echo $topic->name; ?></span>
								</a>				
							</div>

						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</section>
			
		</div>
	</div>	