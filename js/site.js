$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){
		$('header, nav').toggleClass('open');
		return false;
	});

	// Menu Toggle
	$('.all .view-all').click(function(){
		$('.all .modal').toggleClass('show');

      $(this).text(function(i, text){
          return text === "View all countries" ? "Hide" : "View all countries";
      })
		return false;
	});


	$('.share .link img').click( function() {
		$(this).siblings('span').fadeToggle(200);

		return false;
	});

	$('.mobile-categories h3').on('click', function(){

		$('.mobile-categories .list').slideToggle('fast');
		$(this).toggleClass('active');

		return false;
	});

			

	// Find all YouTube videos
	var $allVideos = $("iframe[src*='youtube.com']"),

	    // The element that is fluid width
	    $fluidEl = $('.article-body');

	// Figure out and save aspect ratio for each video
	$allVideos.each(function() {

	  $(this)
	    .data('aspectRatio', this.height / this.width)

	    // and remove the hard coded width/height
	    .removeAttr('height')
	    .removeAttr('width');

	});

	// When the window is resized
	$(window).resize(function() {

	  var newWidth = $fluidEl.width();

	  // Resize all videos according to their own aspect ratio
	  $allVideos.each(function() {

	    var $el = $(this);
	    $el
	      .width(newWidth)
	      .height(newWidth * $el.data('aspectRatio'));

	  });

	// Kick off one resize to fix all videos on page load
	}).resize();

});