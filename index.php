<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<section id="content">

				<?php get_template_part('partials/content-header'); ?>

				<?php if (have_posts()): ?>

					<div class="posts">

						<?php while (have_posts()): the_post(); ?>

							<?php get_template_part('partials/post'); ?>

						<?php endwhile; ?>

						<?php get_template_part('partials/pagination'); ?>

					</div>

				<?php else: ?>

					<div class="no-results">
						<h2>No posts found</h2>
					</div>

				<?php endif; ?>
				
			</section>

			<?php get_sidebar(); ?>

		</div>
	</section>
	
<?php get_footer(); ?>