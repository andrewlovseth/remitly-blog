<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/
add_theme_support( 'post-thumbnails' );
show_admin_bar( false );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_tiled_gallery_width() {
    return '1200';
}
add_filter( 'tiled_gallery_content_width', 'custom_tiled_gallery_width' );

add_theme_support( 'title-tag' );




/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );






/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

/* Filter <p>'s on <img> and <iframe>' */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('acf/format_value/type=wysiwyg', 'filter_ptags_on_images', 10, 3);



function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    .inline-edit-author .title {
      visibility: hidden;
    }
    .inline-edit-author .title:before {
      content: "Editor";
      visibility: visible;
    } 
  </style>';
}









/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}



  $languages = array( 'en', 'es' );
  foreach ( $languages as $lang ) {
    acf_add_options_page( array(
      'page_title' => 'Site Options (' . strtoupper( $lang ) . ')',
      'menu_title' => __('Site Options (' . strtoupper( $lang ) . ')', 'text-domain'),
      'menu_slug'  => "site-options-${lang}",
      'post_id'    => $lang
    ) );
  }





// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');