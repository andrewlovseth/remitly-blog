
	<?php if(pll_current_language() == 'en'): ?>
		<?php get_template_part('partials/footer-en'); ?>
	<?php elseif(pll_current_language() == 'es'): ?>
		<?php get_template_part('partials/footer-es'); ?>
	<?php endif; ?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>