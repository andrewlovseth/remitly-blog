<section id="sidebar">

	<section class="home">
		<h3><a href="<?php the_field('blog_url', pll_current_language('slug')); ?>"><?php the_field('blog_home_header', pll_current_language('slug')); ?></a></h3>
	</section>

	<section class="topics">
		<h3><?php the_field('topics_header', pll_current_language('slug')); ?></h3>

		<?php $topics = get_field('topics', pll_current_language('slug')); if( $topics ): ?>
			<?php foreach( $topics as $topic ): ?>

				<div class="topic">
					<a href="<?php echo get_term_link( $topic->term_id ); ?>">
						<span class="name"><?php echo $topic->name; ?></span>
					</a>				
				</div>

			<?php endforeach; ?>
		<?php endif; ?>
	</section>

	<section class="places">
		<h3><?php the_field('places_header', pll_current_language('slug')); ?></h3>

		<?php $places = get_field('places', pll_current_language('slug')); if( $places ): ?>
			<?php foreach( $places as $place ): ?>

				<div class="place">
					<a href="<?php echo get_term_link( $place->term_id ); ?>">
						<img src="<?php $image = get_field('flag', 'country_' . $place->term_id); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<span class="name"><?php echo $place->name; ?></span>
					</a>				
				</div>

			<?php endforeach; ?>
		<?php endif; ?>

		<div class="all">
			<a href="#" class="view-all">View all countries</a>

			<div class="modal">
				<?php 				
				$terms = get_terms( array(
				    'taxonomy' => 'country',
				    'hide_empty' => true,
				) );
				?>		

				<?php if (!empty($terms) && !is_wp_error($terms) ): ?>

					<div class="term-wrapper">

						<?php foreach ( $terms as $term ): ?>
							<div class="term">
								<a href="<?php echo get_term_link( $term ); ?>">
									<img src="<?php $image = get_field('flag', 'country_' . $term->term_id); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									<span class="name"><?php echo $term->name; ?></span>
								</a>
							</div>

						<?php endforeach; ?>
						
					</div>

				<?php endif; ?>

			</div>
		</div>
	</section>


	<section class="search">
		<h3><?php the_field('search_header', pll_current_language('slug')); ?></h3>

		<?php echo get_search_form(); ?>
	</section>



	<section class="recent-posts">
		<h3><?php the_field('recent_posts_header', pll_current_language('slug')); ?></h3>

			<?php
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 3
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

				<div class="post">
					<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>


				</div>

			<?php endwhile; endif; wp_reset_postdata(); ?>		
	</section>

	<section class="featured-posts">
		<h3><?php the_field('featured_posts_header', pll_current_language('slug')); ?></h3>

		<?php $posts = get_field('featured_posts', pll_current_language('slug')); if( $posts ): ?>
			<?php foreach( $posts as $p ): ?>
				<div class="post">
					<div class="photo">
						<a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_post_thumbnail($p->ID, 'medium'); ?></a>			
					</div>

					<div class="info">
						<h4><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h4>
					</div></div>
			<?php endforeach; ?>
		<?php endif; ?>


	</section>

</section>