<?php

/*

	Template Name: Home

*/

get_header(); ?>


	<section id="main">
		<div class="wrapper">

			<section id="content">

				<section id="content-header">
					<h1><?php the_field('headline'); ?></h1>
					<h2><?php the_field('tagline'); ?></h2>
				</section>

				<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
 

				    <?php if( get_row_layout() == 'one_col' ): ?>

					    <section class="one-col">

						    <?php $post_object = get_sub_field('article_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						    	<?php get_template_part('partials/sections/one-col-article'); ?>

						    <?php wp_reset_postdata(); endif; ?>
																	    	
					    </section>

				    <?php endif; ?>


				    <?php if( get_row_layout() == 'two_col' ): ?>

				    	<section class="two-col">

				    		<?php if(get_sub_field('header')): ?>
				    			<div class="header">
				    				<h3><?php the_sub_field('header'); ?></h3>
				    			</div>
					    	<?php endif; ?>

						    <?php $post_object = get_sub_field('article_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						    	<?php get_template_part('partials/sections/two-col-article'); ?>

						    <?php wp_reset_postdata(); endif; ?>

						    <?php $post_object = get_sub_field('article_b'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						    	<?php get_template_part('partials/sections/two-col-article'); ?>

						    <?php wp_reset_postdata(); endif; ?>
				    		
				    	</section>
												
				    <?php endif; ?>

				 
				    <?php if( get_row_layout() == 'three_col' ): ?>

				    	<section class="three-col">

				    		<?php if(get_sub_field('header')): ?>
				    			<div class="header">
				    				<h3><?php the_sub_field('header'); ?></h3>
				    			</div>
					    	<?php endif; ?>

						    <?php $post_object = get_sub_field('article_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						    	<?php get_template_part('partials/sections/three-col-article'); ?>

						    <?php wp_reset_postdata(); endif; ?>

						    <?php $post_object = get_sub_field('article_b'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						    	<?php get_template_part('partials/sections/three-col-article'); ?>

						    <?php wp_reset_postdata(); endif; ?>

						    <?php $post_object = get_sub_field('article_c'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						    	<?php get_template_part('partials/sections/three-col-article'); ?>

						    <?php wp_reset_postdata(); endif; ?>
				    		
				    	</section>
												
				    <?php endif; ?>				 


				    <?php if( get_row_layout() == 'category_feed' ): ?>

						<?php get_template_part('partials/sections/cat'); ?>
												
				    <?php endif; ?>		
	

				    <?php if( get_row_layout() == 'tag_feed' ): ?>

						<?php get_template_part('partials/sections/tag'); ?>
												
				    <?php endif; ?>		
				    
	
				    <?php if( get_row_layout() == 'country_feed' ): ?>'

						<?php get_template_part('partials/sections/country'); ?>
												
				    <?php endif; ?>		


				<?php endwhile; endif; ?>

			</section>

			<?php get_sidebar(); ?>

		</div>
	</section>

<?php get_footer(); ?>